const canvas = document.querySelector("canvas");
const ctx = canvas.getContext("2d");
canvas.width = window.innerWidth;
canvas.height = window.innerWidth / 2;
let cw = canvas.width;
let ch = canvas.height;

let newGame = true;
let pause = false;
let playersBallHitings = -1;
let totalPlayersBallHitings = 0;
const gsmeStartDelay = 3000;
const ballSize = 20;
let ballX = (cw / 2) - (ballSize / 2);
let ballY = (ch / 2) - (ballSize / 2);
const paddleWeigth = 20;
let paddleHeight = 100;
const playerX = 30;
let playerY = 200;
let computerX = cw - 30 - paddleWeigth;
let computerY = 200;
const lineWidth = 6;
const lineHeight = 16;
const speedUpY = 0.2;
const speedUpX = 0.4;
let ballSpeedX = 4;
let ballSpeedY = 4;

function player() {
  ctx.fillStyle = "grey";
  ctx.fillRect(playerX, playerY, paddleWeigth, paddleHeight);
}

function computer() {
  computerPosition();
  ctx.fillStyle = "grey";
  ctx.fillRect(computerX, computerY, paddleWeigth, paddleHeight);
}

function restartCountDown() {

  let elm3 = document.querySelector(".count3");
  let newone3 = elm3.cloneNode(true);
  elm3.parentNode.replaceChild(newone3, elm3);

  let elm2 = document.querySelector(".count2");
  let newone2 = elm2.cloneNode(true);
  elm2.parentNode.replaceChild(newone2, elm2);

  let elm1 = document.querySelector(".count1");
  let newone1 = elm1.cloneNode(true);
  elm1.parentNode.replaceChild(newone1, elm1);
}

function ball() {
  newGame = false;
  ctx.fillStyle = "grey";
  ctx.fillRect(ballX, ballY, ballSize, ballSize);
  ballX += ballSpeedX;
  ballY += ballSpeedY;
  if (ballY <= 0 || ballY + ballSize >= ch) {
    ballSpeedY = -ballSpeedY;
    speedUp();
  }
  if (ballX <= 0 || ballX + ballSize >= cw) {
    restartCountDown();
    newGame = true;
    totalPlayersBallHitings +=playersBallHitings;
    playersBallHitings =-1;

    ballSpeedX = 4;
    ballSpeedY = 4;
    ballX = (cw / 2) - (ballSize / 2);
    ballY = (ch / 2) - (ballSize / 2);
  }

  if (ballSpeedX < 0 &&
    ballY >= playerY &&
    ballY + ballSize <= playerY + paddleHeight &&
    ballX <= playerX + paddleWeigth &&
    ballX >= playerX
  ) {
    ballSpeedX = -ballSpeedX;
    speedUp();
    hitings();
  }

  if (ballSpeedX > 0 &&
    ballY >= computerY &&
    ballY + ballSize <= computerY + paddleHeight &&
    ballX + ballSize >= computerX &&
    ballX <= computerX
  ) {
    ballSpeedX = -ballSpeedX;
    speedUp();
  }
}

function table() {
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, cw, ch);
  for (let linePosition = 20; linePosition < ch; linePosition += 30) {
    ctx.fillStyle = "grey";
    ctx.fillRect(cw / 2 - lineWidth / 2, linePosition, lineWidth, lineHeight);
  }
}

// dzwiek i animacja przy odbicu

function delay() {
  hitings();
  let time = new Date().getTime();
  let delay = time + gsmeStartDelay;
  while (delay > time) {
    time = new Date().getTime();
  }
}

function resize() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerWidth / 2;
  cw = canvas.width;
  ch = canvas.height;
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, cw, ch);
}

function playerPosition(e) {
  playerY = (e.offsetY) - paddleHeight / 2;
  if (playerY <= 0) {
    playerY = 0;
  }
  if (playerY >= ch - paddleHeight) {
    playerY = ch - paddleHeight;
  }
}

function computerPosition() {
  computerX = cw - 30 - paddleWeigth;
  computerY = ballY - paddleHeight / 2 + ballSize / 2;
  if (computerY <= 0) {
    computerY = 0;
  }
  if (computerY >= ch - paddleHeight) {
    computerY = ch - paddleHeight;
  }
}

// zmienic na cos innego
function speedUp() {
  if (ballSpeedX > 0 && ballSpeedX < 16) {
    ballSpeedX += speedUpX;
  }
  if (ballSpeedX < 0 && ballSpeedX > -16) {
    ballSpeedX -= speedUpX;
  }
  if (ballSpeedY > 0 && ballSpeedY < 11) {
    ballSpeedY += speedUpY;
  }
  if (ballSpeedY < 0 && ballSpeedY > -11) {
    ballSpeedY -= speedUpY;
  }
}

function paddlePlus(){
  if(paddleHeight+60<ch){
    paddleHeight += 20;
  }
}

function paddleMinus(){
  if(paddleHeight>=60){
    paddleHeight -= 20;
  }
}

function hitings() {
  playersBallHitings++;
  document.getElementById("hitings").innerHTML="Odbicia: "+playersBallHitings;
  document.getElementById("total-hitings").innerHTML="Suma odbić: "+totalPlayersBallHitings;
}

function gamePause() {
  if (!pause) {
    pause = true
  } else {
    pause = false
  }
}

canvas.addEventListener("mousemove", playerPosition);
window.addEventListener("resize", resize);
canvas.addEventListener("click", gamePause);

table();
player();
computer();

function game() {
  if (newGame) {
    delay();
  }
  if (!pause) {
    table();
    ball();
    player();
    computer();
  }
}

setInterval(game, 1000 / 60);